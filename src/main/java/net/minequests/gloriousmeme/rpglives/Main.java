package net.minequests.gloriousmeme.rpglives;

import net.minequests.gloriousmeme.rpglives.commands.LivesCommand;
import net.minequests.gloriousmeme.rpglives.commands.RPGLivesCommand;
import net.minequests.gloriousmeme.rpglives.events.LifeItemInteract;
import net.minequests.gloriousmeme.rpglives.events.LifeItemPlace;
import net.minequests.gloriousmeme.rpglives.events.PlayerDeath;
import net.minequests.gloriousmeme.rpglives.events.PlayerJoin;
import net.minequests.gloriousmeme.rpglives.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;

public class Main extends JavaPlugin {

    public static File livesf;
    public static FileConfiguration livesl;
    /*
    TODO: separate file to store lives
    TODO: Per user life regen (Maybe)
    TODO: implement xp saving
    TODO: implement a permission system to determine their max lives
    TODO: implement a hashmap loading system instead of just using config
    TODO: Add ability to select specific slots you lose on pve / pvp death (Maybe)
     */

    public void onEnable() {

        registerConfig();
        registerCommands();
        registerEvents();
        createFiles();

        new BukkitRunnable() {
            public void run() {
                if (!getConfig().getBoolean("LifeRegen")) {
                    return;
                }
                for (Player player : Bukkit.getServer().getOnlinePlayers()) {
                    if (getLives(player) > getMaxLives(player)) {
                        setLives(player, getMaxLives(player));
                        return;
                    }
                    if (getLives(player) < getMaxLives(player))
                    {
                        int i = getLives(player);
                        i++;
                        setLives(player, i);
                        player.sendMessage(Utils.replaceColors(getConfig().getString("GainLifeMessage").replaceAll("<lives>", String.valueOf(getLives(player)))).replaceAll("<maxlives>", String.valueOf(getMaxLives(player))));
                    }
                }
            }
        }.runTaskTimerAsynchronously(this, 0, getConfig().getInt("LifeCooldown") * 1200);
    }

    public void onDisable() {
        for (Map.Entry<UUID, Integer> entry : Utils.lives.entrySet()) {
            livesl.set(entry.getKey() + ".lives", entry.getValue());
        }
        for (Map.Entry<UUID, Integer> entry : Utils.maxlives.entrySet()) {
            livesl.set(entry.getKey() + ".maxlives", entry.getValue());
        }
        try {
            livesl.save(livesf);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void registerConfig() {
        saveDefaultConfig();
        getConfig().options().copyDefaults(true);
    }

    public void registerCommands() {
        getCommand("lives").setExecutor(new LivesCommand(this));
        getCommand("rpglives").setExecutor(new RPGLivesCommand(this));
    }

    public void registerEvents() {
        PluginManager pm = Bukkit.getServer().getPluginManager();
        pm.registerEvents(new PlayerJoin(this), this);
        pm.registerEvents(new PlayerDeath(this), this);
        pm.registerEvents(new LifeItemInteract(this), this);
        pm.registerEvents(new LifeItemPlace(this), this);
    }

    public int getLives(Player player) {
        return Utils.lives.get(player.getUniqueId());
    }

    public int getMaxLives(Player player) {
        return Utils.maxlives.get(player.getUniqueId());
    }

    public int getConfigLives(Player player) {
        return livesl.getInt(player.getUniqueId() + ".lives");
    }

    public int getConfigMaxLives(Player player) {
        return livesl.getInt(player.getUniqueId() + ".maxlives");
    }

    public void setLives(Player player, int i) {
        Utils.lives.put(player.getUniqueId(), i);
        player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 1);
    }

    public void setMaxLives(Player player, int i) {
        Utils.maxlives.put(player.getUniqueId(), i);
    }

    private void createFiles() {
        livesf = new File(getDataFolder(), "lives.yml");

        if (!livesf.exists()) {
            livesf.getParentFile().mkdirs();
            saveResource("lives.yml", false);
        }
        livesl = new YamlConfiguration();
        livesl = new YamlConfiguration();
        try {
            livesl.load(livesf);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }
}