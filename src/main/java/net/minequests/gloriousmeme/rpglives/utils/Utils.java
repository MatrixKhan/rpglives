package net.minequests.gloriousmeme.rpglives.utils;

import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

public class Utils {

    public static HashMap<UUID, Integer> lives = new HashMap<>();
    public static HashMap<UUID, Integer> maxlives = new HashMap<>();

    public static String replaceColors(String message) {
        String string = message.replaceAll("&" , "§");
        return string.replaceAll("§§" , "&");
    }

    public static boolean isNumber(String string) {
        try {
            Integer.parseInt(string);
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }

    public static void clearArmor(Player player) {
        player.getInventory().setHelmet(null);
        player.getInventory().setChestplate(null);
        player.getInventory().setLeggings(null);
        player.getInventory().setBoots(null);
    }
}