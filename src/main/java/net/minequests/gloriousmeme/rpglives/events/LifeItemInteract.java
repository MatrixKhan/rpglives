package net.minequests.gloriousmeme.rpglives.events;

import net.minequests.gloriousmeme.rpglives.Main;
import net.minequests.gloriousmeme.rpglives.utils.Utils;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class LifeItemInteract implements Listener {

    final Main plugin;

    public LifeItemInteract(Main plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();

        if (event.getAction() == Action.RIGHT_CLICK_AIR || (event.getAction() == Action.RIGHT_CLICK_BLOCK)) {
            if (event.getItem() == null) {
                return;
            }
            if (!event.getItem().hasItemMeta()) {
                return;
            }
            if (event.getItem().getItemMeta().getDisplayName() == null) {
                return;
            }
            if (!event.getItem().getItemMeta().getDisplayName().equalsIgnoreCase(Utils.replaceColors(plugin.getConfig().getString("LifeItemName")))) {
                return;
            }
            if (plugin.getLives(player) >= plugin.getMaxLives(player)) {
                return;
            }
            ItemStack hand = event.getItem();
            int amount = hand.getAmount();

            if (amount > 1) {
                int i = plugin.getLives(player);
                i++;
                hand.setAmount(amount - 1);
                player.getInventory().setItemInMainHand(hand);
                plugin.setLives(player, i);
                player.sendMessage(Utils.replaceColors(plugin.getConfig().getString("GainLifeMessage").replaceAll("<lives>", String.valueOf(plugin.getLives(player)))).replaceAll("<maxlives>", String.valueOf(plugin.getMaxLives(player))));
                return;
            } else {
                int i = plugin.getLives(player);
                i++;
                player.getInventory().setItemInMainHand(new ItemStack(Material.AIR));
                plugin.setLives(player, i);
                player.sendMessage(Utils.replaceColors(plugin.getConfig().getString("GainLifeMessage").replaceAll("<lives>", String.valueOf(plugin.getLives(player)))).replaceAll("<maxlives>", String.valueOf(plugin.getMaxLives(player))));
                return;
            }
        }
    }
}