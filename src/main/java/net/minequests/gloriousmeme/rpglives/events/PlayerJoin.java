package net.minequests.gloriousmeme.rpglives.events;

import net.minequests.gloriousmeme.rpglives.Main;
import net.minequests.gloriousmeme.rpglives.utils.Utils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import static net.minequests.gloriousmeme.rpglives.Main.livesl;

public class PlayerJoin implements Listener {

    final Main plugin;

    public PlayerJoin(Main plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        final Player player = event.getPlayer();
        if (!livesl.contains(player.getUniqueId().toString())) {
            Utils.lives.put(player.getUniqueId(), plugin.getConfig().getInt("AmountOfLives"));
            Utils.maxlives.put(player.getUniqueId(), plugin.getConfig().getInt("AmountOfLives"));
            return;
        } else {
            Utils.lives.put(player.getUniqueId(), plugin.getConfigLives(player));
            Utils.maxlives.put(player.getUniqueId(), plugin.getConfigMaxLives(player));
        }
    }
}