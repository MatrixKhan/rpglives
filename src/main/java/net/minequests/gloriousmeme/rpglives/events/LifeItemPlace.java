package net.minequests.gloriousmeme.rpglives.events;

import net.minequests.gloriousmeme.rpglives.Main;
import net.minequests.gloriousmeme.rpglives.utils.Utils;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

public class LifeItemPlace implements Listener {

    final Main plugin;

    public LifeItemPlace(Main plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {

        if (!event.getItemInHand().hasItemMeta()) {
            return;
        }
        if (event.getItemInHand().getItemMeta().getDisplayName() == null) {
            return;
        }
        if (event.getItemInHand().getItemMeta().getDisplayName().equalsIgnoreCase(Utils.replaceColors(plugin.getConfig().getString("LifeItemName")))) {
            event.setCancelled(true);
            return;
        }
    }
}
