package net.minequests.gloriousmeme.rpglives.events;

import net.minequests.gloriousmeme.rpglives.Main;
import net.minequests.gloriousmeme.rpglives.utils.Utils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public class PlayerDeath implements Listener {

    final Main plugin;

    public PlayerDeath(Main plugin) {
        this.plugin = plugin;
    }

    private HashMap<Player, ItemStack[]> invsave = new HashMap<>();
    private HashMap<Player, ItemStack[]> armorsave = new HashMap<>();

    @EventHandler
    public void onRespawn(PlayerRespawnEvent event) {
        Player player = event.getPlayer();
        if (invsave.containsKey(player) && armorsave.containsKey(player)) {
            player.getInventory().setContents(invsave.get(player));
            player.getInventory().setArmorContents(armorsave.get(player));
            invsave.remove(player);
            armorsave.remove(player);
            return;
        }
    }

    @EventHandler
    public void onDeath(EntityDeathEvent event) {
        if (!(event.getEntity() instanceof Player)) {
            return;
        }

        Player player = (Player) event.getEntity();
        if (plugin.getLives(player) <= 0) {
            return;
        }
        if (!plugin.getConfig().getStringList("Worlds").contains(player.getWorld().getName())) {
            return;
        }
        if (player.getKiller() instanceof Player) {
            int x = plugin.getLives(player) - plugin.getConfig().getInt("PVPDeathAmount");

            plugin.setLives(player, x);
            invsave.put(player, player.getInventory().getContents());
            armorsave.put(player, player.getInventory().getArmorContents());
            player.getInventory().clear();
            Utils.clearArmor(player);
            event.getDrops().clear();
            player.sendMessage(Utils.replaceColors(plugin.getConfig().getString("LostLifeMessage").replaceAll("<lives>", String.valueOf(plugin.getLives(player)))).replaceAll("<maxlives>", String.valueOf(plugin.getMaxLives(player))));
            return;
        } else {
            int i = plugin.getLives(player) - plugin.getConfig().getInt("NormalDeathAmount");

            plugin.setLives(player, i);
            invsave.put(player, player.getInventory().getContents());
            armorsave.put(player, player.getInventory().getArmorContents());
            player.getInventory().clear();
            Utils.clearArmor(player);
            event.getDrops().clear();
            player.sendMessage(Utils.replaceColors(plugin.getConfig().getString("LostLifeMessage").replaceAll("<lives>", String.valueOf(plugin.getLives(player)))).replaceAll("<maxlives>", String.valueOf(plugin.getMaxLives(player))));
            return;
        }
    }
}
