package net.minequests.gloriousmeme.rpglives.commands;

import net.minequests.gloriousmeme.rpglives.Main;
import net.minequests.gloriousmeme.rpglives.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class RPGLivesCommand implements CommandExecutor {

    final Main plugin;

    public RPGLivesCommand(Main plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 0 || args[0].equalsIgnoreCase("help")) {
            if (!sender.hasPermission("rpglives.help")) {
                sender.sendMessage(Utils.replaceColors(plugin.getConfig().getString("NoPermsMessage")));
                return false;
            }
            sender.sendMessage(ChatColor.GOLD + "Usage: /rpglives help");
            sender.sendMessage(ChatColor.GOLD + "Usage: /rpglives reload");
            sender.sendMessage(ChatColor.GOLD + "Usage: /rpglives giveitem <player> <amount>");
            sender.sendMessage(ChatColor.GOLD + "Usage: /rpglives setlives <player> <amount>");
            sender.sendMessage(ChatColor.GOLD + "Usage: /rpglives setmaxlives <player> <amount>");
            sender.sendMessage(ChatColor.GOLD + "Usage: /lives");
            return true;
        }
        if (args[0].equalsIgnoreCase("setmaxlives")) {
            if (!sender.hasPermission("rpglives.setmaxlives")) {
                sender.sendMessage(Utils.replaceColors(plugin.getConfig().getString("NoPermsMessage")));
                return false;
            }
            if (args.length < 3) {
                sender.sendMessage("§4Usage: /rpglives setmaxlives <player> <amount>");
                return false;
            }
            Player target = Bukkit.getServer().getPlayerExact(args[1]);
            if (target == null) {
                sender.sendMessage("§4That player is not online.");
                return false;
            }
            if (!Utils.isNumber(args[2])) {
                sender.sendMessage("§4That is not a number please use a number.");
                return false;
            }
            if (Integer.valueOf(args[2]) <= 0) {
                sender.sendMessage("§4You can not set a player's max lives lower than 1");
                return false;
            }
            sender.sendMessage("§aYou set " + target.getName() + "'s max lives to " + args[2] + ".");
            plugin.setMaxLives(target, Integer.valueOf(args[2]));
            return true;
        }
        if (args[0].equalsIgnoreCase("setlives")) {
            if (!sender.hasPermission("rpglives.setlives")) {
                sender.sendMessage(Utils.replaceColors(plugin.getConfig().getString("NoPermsMessage")));
                return false;
            }
            if (args.length < 3) {
                sender.sendMessage("§4Usage: /rpglives setlives <player> <amount>");
                return false;
            }
            Player target = Bukkit.getServer().getPlayerExact(args[1]);
            if (target == null) {
                sender.sendMessage("§4That player is not online.");
                return false;
            }
            if (!Utils.isNumber(args[2])) {
                sender.sendMessage("§4That is not a number please use a number.");
                return false;
            }
            if (Integer.valueOf(args[2]) > plugin.getMaxLives(target)) {
                sender.sendMessage("§4You can not set a player's lives higher than their max lives in the config.");
                return false;
            }
            sender.sendMessage("§aYou set " + target.getName() + "'s lives to " + args[2] + ".");
            plugin.setLives(target, Integer.valueOf(args[2]));
            return true;
        }
        if (args[0].equalsIgnoreCase("reload")) {
            if (!sender.hasPermission("rpglives.reload")) {
                sender.sendMessage(Utils.replaceColors(plugin.getConfig().getString("NoPermsMessage")));
                return false;
            }
            sender.sendMessage(ChatColor.GREEN + "The RPGLives config has been reloaded.");
            plugin.reloadConfig();
            return true;
        }
        if (args[0].equalsIgnoreCase("giveitem")) {
            if (!sender.hasPermission("rpglives.giveitem")) {
                sender.sendMessage(Utils.replaceColors(plugin.getConfig().getString("NoPermsMessage")));
                return false;
            }
            if (args.length < 3) {
                sender.sendMessage(ChatColor.DARK_RED + "Usage: /rpglives giveitem <player> <amount>");
                return false;
            }
            Player target = Bukkit.getPlayerExact(args[1]);
            if (target == null) {
                sender.sendMessage(ChatColor.DARK_RED + "Player not found.");
                return false;
            }
            if (!Utils.isNumber(args[2])) {
                sender.sendMessage(Utils.replaceColors("&4The amount must be a number."));
                return false;
            }
            int itemAmount = Integer.parseInt(args[2]);
            ItemStack rpgItem = new ItemStack(Material.valueOf(plugin.getConfig().getString("LifeItemType")), itemAmount);
            ItemMeta rpgItemMeta = rpgItem.getItemMeta();
            rpgItemMeta.setDisplayName(Utils.replaceColors(plugin.getConfig().getString("LifeItemName")));
            rpgItemMeta.setLore(plugin.getConfig().getStringList("LifeItemLore"));
            rpgItem.setItemMeta(rpgItemMeta);

            if (itemAmount == 1) {
                sender.sendMessage(Utils.replaceColors("&aYou have given " + target.getName() + " " + args[2] + " " + plugin.getConfig().getString("LifeItemName")));
                target.getInventory().addItem(rpgItem);
                return true;
            } else {
                sender.sendMessage(Utils.replaceColors("&aYou have given " + target.getName() + " " + args[2] + " " + plugin.getConfig().getString("LifeItemName") + "s"));
                target.getInventory().addItem(rpgItem);
                return true;
            }
        }
        return false;
    }
}
