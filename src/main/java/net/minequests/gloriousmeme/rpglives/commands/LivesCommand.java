package net.minequests.gloriousmeme.rpglives.commands;

import net.minequests.gloriousmeme.rpglives.Main;
import net.minequests.gloriousmeme.rpglives.utils.Utils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class LivesCommand implements CommandExecutor {

    final Main plugin;

    public LivesCommand(Main plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("You must be a player to use this command");
            return false;
        }
        Player player = (Player) sender;
        player.sendMessage(Utils.replaceColors(plugin.getConfig().getString("LivesCommandMessage").replaceAll("<lives>", String.valueOf(plugin.getLives(player))).replaceAll("<maxlives>", String.valueOf(plugin.getMaxLives(player)))));
        return true;
    }
}
